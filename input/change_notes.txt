C3H6 + OH = allyl-alcohol + H -7.89E+14	-4.50E-01	10996
---->
C3H6 + OH = allyl-alcohol + H 7.89E+14	-4.50E-01	10996

C3H6 + OH = allyl + H2O	-1.24E8	1.73	925	
---->
C3H6 + OH = allyl + H2O	1.24E8	1.73	925	

C3H6 + OH = ethenol + CH3	2.45E+07	1.55	2310	
!PLOG/	0	2.45E+07	1.55	2310/
PLOG/	0.0013	6.69E+07	1.42	2708/
PLOG/	0.01	3.76E+08	1.21	3068/
PLOG/	0.013	1.20E+09	1.06	3326/
PLOG/	0.025	1.63E+10	0.72	3950/
PLOG/	0.1	2.09E+12	0.13	5407/
PLOG/	0.1315	5.13E+12	0.02	5723/
PLOG/	1	8.73E+20	-2.35	11290/
PLOG/	10	2.27E-02	4.03	1952/
PLOG/	100	6.50E+22	-2.58	19256/
dup				
				<Problem here>
C3H6 + OH = ethenol + CH3	1.30E+04	2.11	308	
!PLOG/	0	1.30E+04	2.11	308/
PLOG/	0.0013	1.29E+06	1.65	1233/
PLOG/	0.01	1.82E+04	2.1	1162/
PLOG/	0.013	2.04E+03	2.48	1128/
PLOG/	0.025	2.88E+02	2.8	1152/
PLOG/	0.1	1.40E+01	3.21	1208/
PLOG/	0.1315	7.71E+00	3.29	1216/
PLOG/	1	1.13E+04	2.5	3238/
PLOG/	10	2.42E+19	-1.74	13107/
PLOG/	100	3.30E-01	3.7	3665/
dup				

Between these two entries, there are four <TAB>s, not empty line

Same for following reaction
C3H6 + OH = propen1ol + H	1.74E+10	0.69	6864	
!PLOG/	0	1.74E+10	0.69	6864/
PLOG/	0.0013	6.50E+10	0.53	7292/
PLOG/	0.01	2.27E+10	0.66	6968/
PLOG/	0.013	1.81E+10	0.69	6884/
PLOG/	0.025	2.07E+10	0.68	6899/
PLOG/	0.1	3.13E+11	0.36	7785/
PLOG/	0.1315	7.29E+11	0.26	8071/
PLOG/	1	2.91E+15	-0.74	11079/
PLOG/	10	4.51E+19	-1.86	15763/
PLOG/	100	3.79E+21	-2.3	20501/
dup				
				<Problem here>
C3H6 + OH = propen1ol + H	1.76E+06	1.57	4133	
!PLOG/	0	1.76E+06	1.57	4133/
PLOG/	0.0013	3.48E+06	1.53	4288/
PLOG/	0.01	1.08E+07	1.34	4576/
PLOG/	0.013	9.76E+06	1.33	4589/
PLOG/	0.025	5.14E+06	1.36	4594/
PLOG/	0.1	3.13E+05	1.69	4603/
PLOG/	0.1315	1.39E+05	1.8	4603/
PLOG/	1	1.03E+02	2.83	4530/
PLOG/	10	3.40E-02	3.89	4390/
PLOG/	100	4.46E-06	5.03	4132/
dup				

Same for following reaction
C3H6 + OH = propen2ol + H	5.13E+03	2.42	2447	
!PLOG/	0	5.13E+03	2.42	2447/
PLOG/	0.0013	9.34E+03	2.35	2635/
PLOG/	0.01	4.08E+04	2.17	3048/
PLOG/	0.013	6.44E+04	2.11	3186/
PLOG/	0.025	2.57E+05	1.94	3598/
PLOG/	0.1	1.51E+07	1.44	4816/
PLOG/	0.1315	3.28E+07	1.35	5084/
PLOG/	1	1.55E+10	0.62	7544/
PLOG/	10	1.48E-05	4.75	2168/
PLOG/	100	3.85E+19	-1.85	19219/
dup				
				<Problem here>
C3H6 + OH = propen2ol + H	4.40E-03	3.67	-518	
!PLOG/	0	4.40E-03	3.67	-518/
PLOG/	0.0013	2.87E+00	2.92	625/
PLOG/	0.01	4.84E-01	2.98	704/
PLOG/	0.013	3.13E-01	3.04	721/
PLOG/	0.025	9.34E-03	3.62	677/
PLOG/	0.1	4.64E-05	4.48	687/
PLOG/	0.1315	2.71E-05	4.56	707/
PLOG/	1	7.65E-07	5.05	874/
PLOG/	10	2.64E+15	-0.8	12728/
PLOG/	100	4.87E-04	4.32	4020/
dup				

Same for following reaction
C3H6 + OH = acetaldehyde + CH3	8.91E+21	-4.56	464	
!PLOG/	0	8.91E+21	-4.56	464/
PLOG/	0.0013	-4.44E+07	0.89	540/
PLOG/	0.01	1.87E+19	-2.96	4951/
PLOG/	0.013	2.61E+15	-1.67	3823/
PLOG/	0.025	3.31E+14	-1.29	3996/
PLOG/	0.1	9.46E+14	-1.3	5272/
PLOG/	0.1315	1.61E+15	-1.35	5603/
PLOG/	1	5.17E+16	-1.67	8264/
PLOG/	10	5.13E+18	-2.11	12359/
PLOG/	100	7.41E+19	-2.29	17262/
dup				
				<Problem here>
C3H6 + OH = acetaldehyde + CH3	7.65E+02	2.24	-1676	
!PLOG/	0	7.65E+02	2.24	-1676/
PLOG/	0.0013	6.93E+05	1.49	-536/
PLOG/	0.01	5.94E+03	2.01	-560/
PLOG/	0.013	1.10E+03	2.22	-680/
PLOG/	0.025	1.07E+02	2.5	-759/
PLOG/	0.1	7.83E-01	3.1	-919/
PLOG/	0.1315	3.07E-01	3.22	-946/
PLOG/	1	3.16E-04	4.05	-1144/
PLOG/	10	7.59E-06	4.49	-680/
PLOG/	100	5.45E-05	4.22	1141/
dup				


in file chem_TEST.inp, chem.inp and chem.out, 
to reaction C3H6+OH=allyl+H2O, change pre-factor to 1.00E+00, or delete the negative sign, otherwise it will be compiled to ********
to reaction C3H6+OH=allyl-alcohol+H, change pre-factor to 1.00E+00, or delete negative sign,  otherwise it will be compiled to ********
in file chem.out, these reactions are split into two lines, make them to be one line otherwise C++ kernel cannot read it, C++ kernel uses one line regular BTW
 486. ipropyloo+acetylperoxy                        1.40E+16   -1.6     1860.0
      =ipropyloxy+acetyloxy+O2                                  
 487. npropyloo+acetylperoxy                        1.40E+16   -1.6     1860.0
      =npropyloxy+acetyloxy+O2                                  
